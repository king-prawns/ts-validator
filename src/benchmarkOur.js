"use strict";
exports.__esModule = true;
var sitemapMock = require('./generator/sitemapMock.json');
console.time("Time our");
var benchmark = function () {
    var result = true;
    for (var letter in sitemapMock) {
        if (!(typeof sitemapMock[letter] === 'object')) {
            result = false;
            break;
        }
        // Letters have prop countries, that is an array
        if (!Array.isArray(sitemapMock[letter].letter1)) {
            result = false;
            break;
        }
        // If there are countries check their structure
        if (sitemapMock[letter].letter1.length > 0) {
            for (var _i = 0, _a = sitemapMock[letter].letter1; _i < _a.length; _i++) {
                var country = _a[_i];
                // Countries are Objects
                if (!(typeof country === 'object')) {
                    result = false;
                    break;
                }
                // Check countries props
                if (!(typeof country.country1 === 'string')) {
                    result = false;
                    break;
                }
                if (!Array.isArray(country.country2)) {
                    result = false;
                    break;
                }
                // If there are cities, check their structure
                if (country.country2.length > 0) {
                    for (var _b = 0, _c = country.country2; _b < _c.length; _b++) {
                        var city = _c[_b];
                        // Cities are Objects
                        if (!(typeof city === 'object')) {
                            result = false;
                            break;
                        }
                        // Check cities props
                        if (!(typeof city.city1 === 'string')) {
                            result = false;
                            break;
                        }
                        if (!(typeof city.city2 === 'undefined') && !Array.isArray(city.city2)) {
                            result = false;
                            break;
                        }
                        // If there are venues, check their structure
                        if (city.city2 && city.city2.length > 0) {
                            for (var _d = 0, _e = city.city2; _d < _e.length; _d++) {
                                var venue = _e[_d];
                                // Venues are Objects
                                if (!(typeof venue === 'object')) {
                                    result = false;
                                    break;
                                }
                                // Check venues props
                                if (!(typeof venue.venue1 === 'string')) {
                                    result = false;
                                    break;
                                }
                                if (!(typeof venue.venue2 === 'number')) {
                                    result = false;
                                    break;
                                }
                                if (!(typeof venue.venue3 === 'boolean')) {
                                    result = false;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return result;
};
console.log(benchmark());
console.timeEnd("Time our");
