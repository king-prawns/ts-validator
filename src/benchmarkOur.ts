const sitemapMock = require('./generator/sitemapMock.json')

console.time("Time our");

const benchmark = () => {
  let result = true

  for(let letter in sitemapMock) {
    if (!(typeof sitemapMock[letter] === 'object')) { result = false; break; }
    // Letters have prop countries, that is an array
    if (!Array.isArray(sitemapMock[letter].letter1)) { result = false; break; }
    // If there are countries check their structure
    if (sitemapMock[letter].letter1.length > 0) {
        for(let country of sitemapMock[letter].letter1) {
            // Countries are Objects
            if (!(typeof country === 'object')) { result = false; break; }
            // Check countries props
            if (!(typeof country.country1 === 'string')) { result = false; break; }
            if (!Array.isArray(country.country2)) { result = false; break; }
            // If there are cities, check their structure
            if (country.country2.length > 0) {
                for(let city of country.country2) {
                    // Cities are Objects
                    if (!(typeof city === 'object')) { result = false; break; }
                    // Check cities props
                    if (!(typeof city.city1 === 'string')) { result = false; break; }
                    if (!(typeof city.city2 === 'undefined') && !Array.isArray(city.city2)) { result = false; break; }
                    // If there are venues, check their structure
                    if (city.city2 && city.city2.length > 0) {
                        for(let venue of city.city2) {
                            // Venues are Objects
                            if (!(typeof venue === 'object')) { result = false; break; }
                            // Check venues props
                            if (!(typeof venue.venue1 === 'string')) { result = false; break; }
                            if (!(typeof venue.venue2 === 'number')) { result = false; break; }
                            if (!(typeof venue.venue3 === 'boolean')) { result = false; break; }
                        }
                    }
                }
            }
        }
    }
  }

  return result;
}

benchmark()

console.timeEnd("Time our");
