"use strict";
exports.__esModule = true;
var Either_1 = require("fp-ts/lib/Either");
var Sitemap_1 = require("./types/Sitemap");
var sitemapMock = require('./generator/sitemapMock.json');
console.time("Time lib");
var sitemap = Sitemap_1.Sitemap.decode(sitemapMock);
// isRight(sitemap)
console.log(Either_1.isRight(sitemap));
console.timeEnd("Time lib");
