"use strict";
exports.__esModule = true;
var PathReporter_1 = require("io-ts/lib/PathReporter");
var Either_1 = require("fp-ts/lib/Either");
var pipeable_1 = require("fp-ts/lib/pipeable");
var Sitemap_1 = require("../types/Sitemap");
var sitemapMockError = require('./sitemapMockError.json');
var sitemapMockSuccess = require('./sitemapMockSuccess.json');
console.log('---------- SitemapMock Correct ----------');
var resultSuccess = Sitemap_1.Sitemap.decode(sitemapMockSuccess);
console.log(Either_1.isRight(resultSuccess));
console.log(PathReporter_1.PathReporter.report(resultSuccess));
console.log('---------- SitemapMock Wrong ----------');
var resultError = Sitemap_1.Sitemap.decode(sitemapMockError);
console.log(Either_1.isRight(resultError));
console.log(PathReporter_1.PathReporter.report(resultError));
console.log('---------- Pipe example ----------');
// failure handler
var onLeft = function (errors) { return errors.length + " error(s) found"; };
// success handler
var onRight = function (s) { return "No errors: " + JSON.stringify(s); };
pipeable_1.pipe(resultSuccess, Either_1.fold(onLeft, onRight), console.log);
