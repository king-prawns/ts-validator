import * as t from 'io-ts'
import { PathReporter } from 'io-ts/lib/PathReporter'
import { isRight, fold } from 'fp-ts/lib/Either'
import { pipe } from 'fp-ts/lib/pipeable'
import { Sitemap, SitemapType } from '../types/Sitemap'
const sitemapMockError = require('./sitemapMockError.json')
const sitemapMockSuccess: SitemapType = require('./sitemapMockSuccess.json')

console.log('---------- SitemapMock Correct ----------')

const resultSuccess = Sitemap.decode(sitemapMockSuccess)
console.log(isRight(resultSuccess))
console.log(PathReporter.report(resultSuccess))

console.log('---------- SitemapMock Wrong ----------')

const resultError = Sitemap.decode(sitemapMockError)
console.log(isRight(resultError))
console.log(PathReporter.report(resultError))

console.log('---------- Pipe example ----------')

// failure handler
const onLeft = (errors: t.Errors): string => `${errors.length} error(s) found`

// success handler
const onRight = (s: SitemapType) => `No errors: ${JSON.stringify(s)}`

pipe(
  resultSuccess,
  fold(onLeft, onRight),
  console.log
)
