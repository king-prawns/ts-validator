import { isRight } from 'fp-ts/lib/Either'
import { Sitemap } from './types/Sitemap'
const sitemapMock = require('./generator/sitemapMock.json')

console.time("Time lib");

const sitemap = Sitemap.decode(sitemapMock);
isRight(sitemap)
console.timeEnd("Time lib");
