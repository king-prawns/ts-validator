const fs = require('fs')

const obj = {}

for (let index = 0; index < 100; index++) {
  const key = `letter_${index}`
  obj[key] = {}
  obj[key].letter1 = []

  for (let countries = 0; countries < 100; countries++) {
    const country = {}
    country.country1 = `country ${countries} string`
    country.country2 = []

    for (let cities = 0; cities < 10; cities++) {
      const city = {}
      city.city1 = `city ${cities} string`
      city.city2 = []

      for (let venues = 0; venues < 10; venues++) {
        const venue = {
          venue1: `venue ${venues} string`,
          venue2: venues,
          venue3: true
        }

        city.city2.push(venue)
      }
      country.country2.push(city)
    }
    obj[key].letter1.push(country)
  }
}

var json = JSON.stringify(obj);
fs.writeFile('./src/generator/sitemapMock.json', json, 'utf8', () => { console.log('👍') });
