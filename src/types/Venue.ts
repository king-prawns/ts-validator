import * as t from 'io-ts'

export const Venue = t.type({
  venue1: t.string,
  venue2: t.number,
  venue3: t.boolean
})
