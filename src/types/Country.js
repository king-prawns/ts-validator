"use strict";
exports.__esModule = true;
var t = require("io-ts");
var City_1 = require("./City");
exports.Country = t.type({
    country1: t.string,
    country2: t.array(City_1.City)
});
