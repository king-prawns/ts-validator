import * as t from 'io-ts'
import { Letter } from './Letter'

export const Sitemap = t.record(t.string, Letter)

export type SitemapType = t.TypeOf<typeof Sitemap>


