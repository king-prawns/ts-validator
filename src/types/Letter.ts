import * as t from 'io-ts'
import { Country } from './Country'

export const Letter = t.type({
  letter1: t.array(Country)
})