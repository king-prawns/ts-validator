import * as t from 'io-ts'
import { Venue } from './Venue'

const CityRequiredProps = t.type({
  city1: t.string
})

const CityOptionalProps = t.partial({
  city2: t.array(Venue)
})

export const City = t.intersection([ CityRequiredProps, CityOptionalProps ])