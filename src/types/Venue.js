"use strict";
exports.__esModule = true;
var t = require("io-ts");
exports.Venue = t.type({
    venue1: t.string,
    venue2: t.number,
    venue3: t.boolean
});
