import * as t from 'io-ts'
import { City } from './City'

export const Country = t.type({
  country1: t.string,
  country2: t.array(City)
})