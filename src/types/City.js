"use strict";
exports.__esModule = true;
var t = require("io-ts");
var Venue_1 = require("./Venue");
var CityRequiredProps = t.type({
    city1: t.string
});
var CityOptionalProps = t.partial({
    city2: t.array(Venue_1.Venue)
});
exports.City = t.intersection([CityRequiredProps, CityOptionalProps]);
